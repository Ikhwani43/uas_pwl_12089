<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('modelku');
	}
	
	public function index(){
			$this->load->view('login');
	}

	public function login(){
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
		

		$datapen = array(
			'user' => $user,
			'pass' => $pass,
		);
		$cek = count($this->modelku->getData_khusus("duser", $datapen));

		if($cek >0 ){
			$data_session = array(
				'nama' => $user,
				'status'=> "login"
			);
			$this->session->set_userdata($data_session);

			redirect(base_url()."index.php/hal_admin/");
		} else {
			redirect(base_url());
		}
	}

	public function daftar(){
		$this->load->view('Register');
	}

	// public function Register(){
	// 	$datadaftar = array(
	// 		'user' => $this->input->post('username'),
	// 		'pass' => $this->input->post('password'),
	// 		'nama' => $this->input->post('nama'),
	// 		'gambar' => $this->input->post('userfile'),
	// 	);
	// 	$this->modelku->masuk('duser',$datadaftar);
	// 	redirect(base_url(),'refresh');
	// }
	public function Register()
        {
			$gambar = $_FILES['gambar'];
			$nama = $this->input->post('nama');
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			if($gambar=''){}else
			{
				$config['upload_path']          = './assets/images/Profil_user';
                $config['allowed_types']        = 'gif|jpg|png|JPG|PNG';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('gambar'))
				{
					echo "Gagal Singup";
				}else{
					$gambar=$this->upload->data('file_name');
				}			
			}
			$daftar = array(
				'nama' => $nama,
				'gambar' => $gambar,
				'user' => $user,
				'pass' => $pass				
			);
			$this->modelku->masuk('duser', $daftar);
			redirect(base_url(),'refresh');

		}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
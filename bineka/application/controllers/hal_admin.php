<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class hal_admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('modelku');

        if($this->session->userdata('status') !=="login"){
            redirect(base_url());
        }
	}

	public function index(){
		$databrg = $this->modelku->getData("barang");
		$data2 = array(
			'data' => $databrg
		);
		$this->load->view('index', $data2);
	}

	public function ambil_profil($penunjuk){
		$datapen = array(
			'id' => $penunjuk
		);
		$datause = $this->modelku->getData_khusus("duser", $datapen);
		$datauser = array(
			'user' => $datause
			);
			$this->load->view('profil', $datauser);
	}
	public function ambil_checkout($penunjuk){
		$datapen = array(
			'id' => $penunjuk
		);
		$datacheck = $this->modelku->getData_khusus("checkout", $datapen);
		$datacheckout = array(
			'checkout' => $datacheck
			);
			$this->load->view('checkout', $datacheckout);
	}
	public function ambil_cart($penunjuk){
		$datapen = array(
			'id' => $penunjuk
		);
		$datacar = $this->modelku->getData_khusus("cart", $datapen);
		$datacart = array(
			'cart' => $datacar
			);
			$this->load->view('cart', $datacart);
	}

	public function shop(){
		$databrg = $this->modelku->getData("barang");
		$data2 = array(
			'data' => $databrg
		);
		$this->load->view('shop', $data2);
	}

	public function details($penunjuk){
		$datapen = array(
			'kd_brg' => $penunjuk
		);
		$databrg = $this->modelku->getData_khusus("barang", $datapen);
		$data2 = array(
			'data' => $databrg
			);
			$this->load->view('product-details', $data2);
	}

	public function tambah_cart(){
		$datacart = array(
			'id' => $this->input->post('id'),
			'gambar' => $this->input->post('gambar'),
			'nama_produk' => $this->input->post('nama_produk'),
			'jumlah' => $this->input->post('jumlah'),
			'satuan' => $this->input->post('satuan'),
			'harga' => $this->input->post('harga'),
			'harga' => $this->input->post('harga'),
			'kode_produk' => $this->input->post('kode_produk'),
		);
		$this->modelku->masuk('cart', $datacart);
		redirect(base_url()."index.php/hal_admin/");
	}
	public function checkout($penunjuk){
		$datacart = array(
			'id' => $this->input->post('id'),
			'gambar' => $this->input->post('gambar'),
			'nama_produk' => $this->input->post('nama_produk'),
			'jumlah' => $this->input->post('jumlah'),
			'satuan' => $this->input->post('satuan'),
			'harga_asl' => $this->input->post('harga_asl'),
			'harga_total' => $this->input->post('harga_total'),
			'negara' => $this->input->post('negara'),
			'provinsi' => $this->input->post('provinsi'),
			'kota_kabupaten' => $this->input->post('kota_kabupaten'),
			'kelurahan' => $this->input->post('kelurahan'),
			'alamat_lengkap' => $this->input->post('alamat_lengkap'),
			'post_code' => $this->input->post('post_code'),

		);
		$this->modelku->masuk('checkout', $datacart);
		
		$datapen = array(
			'kode_cart' => $penunjuk
		);
		$this->load->model('modelku');
		$this->modelku->hapus('cart',$datapen);
		redirect(base_url()."index.php/hal_admin/");
	}

	public function payment($penunjuk)
        {
			$gambar = $_FILES['gambar'];
			$id = $this->input->post('id');
			$nama_produk = $this->input->post('nama_produk');
			$jumlah = $this->input->post('jumlah');
			$bank = $this->input->post('bank');
			$nama_lengkap = $this->input->post('nama_lengkap');
			$no_rek = $this->input->post('no_rek');
			$harga_total = $this->input->post('harga_total');
			$kurir = $this->input->post('kurir');
			$alamat = $this->input->post('alamat');
			if($gambar=''){}else
			{
				$config['upload_path']          = './assets/images/payment/';
                $config['allowed_types']        = 'gif|jpg|png|JPG|PNG';

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('gambar'))
				{
					echo "Gagal Singup";
					
				}else{
					$gambar=$this->upload->data('file_name');
				}			
			}
			$daftar = array(
				'id' => $id,
				'gambar' => $gambar,
				'nama_produk' => $nama_produk,
				'jumlah' => $jumlah,				
				'bank' => $bank,				
				'nama_lengkap' => $nama_lengkap,				
				'no_rek' => $no_rek,				
				'harga_total' => $harga_total,				
				'kurir' => $kurir,				
				'alamat' => $alamat,			
			);
			$this->modelku->masuk('payment', $daftar);

			$datapen = array(
				'kode_checkout' => $penunjuk
			);
			$this->load->model('modelku');
			$this->modelku->hapus('checkout',$datapen);
			redirect(base_url()."index.php/hal_admin/");

		}

	public function hapus_cart($penunjuk){
		$datapen = array(
			'kode_cart' => $penunjuk
		);
		$this->load->model('modelku');
		$this->modelku->hapus('cart',$datapen);
		redirect(base_url()."index.php/hal_admin/");

	}
	public function hapus_checkout($penunjuk){
		$datapen = array(
			'kode_checkout' => $penunjuk
		);
		$this->load->model('modelku');
		$this->modelku->hapus('checkout',$datapen);
		redirect(base_url()."index.php/hal_admin/");

	}
}
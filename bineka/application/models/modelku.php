<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modelku extends CI_Model {
    // public function getDataBrg($tabel){
    //     $datanih = $this->db->query(" SELECT * FROM .$tabel");
    //     return $datanih->result_array();
    // }

    public function getData($tabel){
        $database = $this->db->get($tabel);
        return $database->result_array();
    }
    public function masuk($tabel,$data){
        $database = $this->db->insert($tabel,$data);
        return $database;
    }
    public function perbarui($tabel,$data,$where){
        $database = $this->db->update($tabel,$data,$where);
        return $database;
    }
    public function getData_khusus($tabel,$where){
        $database = $this->db->get_where($tabel,$where);
        return $database->result_array();
    }
    public function hapus($tabel,$where){
        $database = $this->db->delete($tabel,$where);
        return $database;
    }
}

<?php $id = $this->session->userdata('nama'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>B-HINEKA</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/prettyPhoto.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/prettyPhoto.css') ?>" rel="stylesheet">
    <!-- <link href="css/price-range.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/price-range.css') ?>"" rel="stylesheet">
    <!-- <link href="css/animate.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/animate.css') ?>" rel="stylesheet">
	<!-- <link href="css/main.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css') ?>">
	<!-- <link href="css/responsive.css" rel="stylesheet"> -->
	<link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/images/ico/apple-touch-icon-72-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/images/ico/apple-touch-icon-57-precomposed.png') ?>">
</head><!--/head-->

<body>
<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +62 821-3827-3818</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> BinekaStore@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="images/logo/logo.jpg" alt="" /></a>
							<a href="index.html"><img src="<?php echo base_url('assets/images/logo/logo.jpg') ?>" alt="" /></a>
						</div>
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									Indonesia
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Singapore</a></li>
									<li><a href="#">USA</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									Rupiah
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_profil/".$id;?>"><i class="fa fa-user"></i> My Account</a></li>
								<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_checkout/".$id;?>"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_cart/".$id;?>"><i class="fa fa-shopping-cart"></i> Cart</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html" class="active">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo base_url()."index.php/hal_admin/shop/".$id;?>">Products</a></li>
										<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_checkout/".$id;?>">Checkout</a></li> 
										<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_cart/".$id;?>">Cart</a></li> 
                                    </ul>
                                </li> 
								<li class="dropdown"><a href="#">My Account<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo base_url()."index.php/hal_admin/ambil_profil/".$id;?>">Profil</a></li>
										<li><a href="<?php echo base_url()."index.php/welcome/logout"; ?>">logout</a></li>
                                    </ul>
                                </li>
								<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog.html">Blog List</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<?php
			$id = '0';
			$kode_checkout  = '0';
			$gambar  = '0';
			$nama_produk  = '0';
			$jumlah  = '0';
			$satuan = '0';
			$harga_asl = '0';
			$harga_total = '0';
			$negara = '0';
			$provinsi = '0';
			$kota_kabupaten = '0';
			$kelurahan = '0';
			$alamat_lengkap = '0';
			$post_code = '0';
		
				foreach ($checkout as $dat){
					$id = $dat['id'];
					$kode_checkout  = $dat['kode_checkout'];
					$gambar  = $dat['gambar'];
					$nama_produk  = $dat['nama_produk'];
					$jumlah  = $dat['jumlah'];
					$satuan = $dat['satuan'];
					$harga_asl = $dat['harga_asl'];
					$harga_total = $dat['harga_total'];
					$negara = $dat['negara'];
					$provinsi = $dat['provinsi'];
					$kota_kabupaten = $dat['kota_kabupaten'];
					$kelurahan = $dat['kelurahan'];
					$alamat_lengkap = $dat['alamat_lengkap'];
					$post_code = $dat['post_code'];
			?>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_product">
								<a href=""><img src="<?php echo base_url("assets/images/home/$gambar")?>" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""><?php echo $nama_produk; ?></a></h4>
							</td>
							<td class="cart_price">
								<p><?php echo $harga_asl; ?></p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href=""> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $jumlah; ?>" autocomplete="off" size="2">
									<a class="cart_quantity_down" href=""> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price"><?php echo $harga_total;?></p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="<?php echo base_url()."index.php/hal_admin/hapus_checkout/".$kode_checkout;?>"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<?php echo form_open_multipart('index./hal_admin/payment/'.$kode_checkout); ?>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_info">
							<li class="single_field">
								<label>Alamat:</label>
								<input type="text" name="alamat" value="<?php echo $negara; ?>,<?php echo $provinsi; ?>,<?php echo $kota_kabupaten; ?>,<?php echo $kelurahan; ?>,<?php echo $alamat_lengkap; ?>,<?php echo $post_code; ?>" required/>
							</li>
							<div class="col-sm-6">
							<label>Bank pengiriman:</label>
							<select name="bank" id="bank">
								<option value="BNi">BNi</option>
								<option value="BRI">BRI</option>
								<option value="Mandiri">Mandiri</option>
								<option value="BCA">BCA</option>
							</select>
							</div>
							<div class="col-sm-6">
							<label>Bank pengiriman:</label>
							<select name="kurir" id="kurir">
								<option value="JNE">JNE</option>
								<option value="JNT">JNT</option>
								<option value="TIKI">TIKI</option>
								<option value="Sicepat">Sicepat</option>
							</select>
							</div>
							<div class="col-6">
							<li class="single_field">
								<label>Bukti Tranfer</label>
								<input type="file" name="gambar"  required/>
							</li>
							</div>
							<div class="col-6">
							<li class="single_field">
								<label>Nama Lengkap</label>
								<input type="text" name="nama_lengkap"  required/>
							</li>
							</div>
							<div class="col-6">
							<li class="single_field">
								<label>Nomer Rekening</label>
								<input type="text" name="no_rek"  required/>
							</li>
							</div>
						</ul>
					</div>
				</div>
				<input type="hidden" name="jumlah" value="<?php echo $jumlah; ?>" />
				<input type="hidden" name="id" value="<?php echo $id; ?>">
				<input type="hidden" name="nama_produk" value="<?php echo $nama_produk; ?>">
				<input type="hidden" name="harga_total" value="<?php echo $harga_total; ?>">
				<div class="col-sm-6">
					<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td><?php echo $harga_total;?></td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>Free</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span><?php echo $harga_total;?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<button type="submit" class="btn btn-fefault cart">
							payment  
						</button>
				</div>
			</div> 
			<?php echo form_close() ?>	 
		</div>
	</section><!--/#do_action-->

	

	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>B</span>-hineka</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>Kami Hadir di Seluruh Pejuru- Dunia(2021 juni, Indonesia, Semarang)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Online Help</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Order Status</a></li>
								<li><a href="#">Change Location</a></li>
								<li><a href="#">FAQ’s</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Mobile</a></li>
								<li><a href="#">LAPTOPS</a></li>
								<li><a href="#">DISPLAYS / DESKTOP</a></li>
								<li><a href="#">MOTHERBOARDS</a></li>
								<li><a href="#">COMPONENTS</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privecy Policy</a></li>
								<li><a href="#">Refund Policy</a></li>
								<li><a href="#">Billing System</a></li>
								<li><a href="#">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Company Information</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
				<p class="pull-left">Copyright © 2021 B-hineka All Rights Reserved By Albertus Dimas.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.Webinaja.com">Albert</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	


    <!-- <script src="js/jquery.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	<!-- <script src="js/bootstrap.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/jquery.scrollUp.min.js') ?>"></script>
	<!-- <script src="js/price-range.js"></script> -->
	<script src="<?php echo base_url('assets/js/price-range.js') ?>"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js') ?>"></script>
    <!-- <script src="js/main.js"></script> -->
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
</body>
</html>
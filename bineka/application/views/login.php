<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>B-HINEKA</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/prettyPhoto.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/prettyPhoto.css') ?>" rel="stylesheet">
    <!-- <link href="css/price-range.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/price-range.css') ?>"" rel="stylesheet">
    <!-- <link href="css/animate.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/animate.css') ?>" rel="stylesheet">
	<!-- <link href="css/main.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css') ?>">
	<!-- <link href="css/responsive.css" rel="stylesheet"> -->
	<link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/images/ico/apple-touch-icon-72-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/images/ico/apple-touch-icon-57-precomposed.png') ?>">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +62 821-3827-3818</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> BinekaStore@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							
						<a href="index.html"><img src="images/logo/logo.jpg" alt="" /></a>
						<a href="index.html"><img src="<?php echo base_url('assets/images/logo/logo.jpg') ?>" alt="" /></a>
						</div>
				</div>
			</div>
		</div><!--/header-middle-->

	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Login to your account!</h2>
						<form action="<?php echo base_url()."index.php/welcome/login"?>" method="POST">
							<input type="text" placeholder="Username" name="username" />
							<input type="password" placeholder="Password" name="password"/>
							<span>
								<h2>Belum Punya Akun?<a href="<?php echo base_url()."index.php/welcome/daftar"?>">Daftar Disini</a></h2>
							</span>
							<button type="submit" class="btn btn-default">Login</button><br>
							<button type="reset" class="btn btn-default">Reset</button> 
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	
	<footer id="footer"><!--Footer-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2021 B-hineka All Rights Reserved By Albertus Dimas.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.Webinaja.com">Albert</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <!-- <script src="js/jquery.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	<!-- <script src="js/bootstrap.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/jquery.scrollUp.min.js') ?>"></script>
	<!-- <script src="js/price-range.js"></script> -->
	<script src="<?php echo base_url('assets/js/price-range.js') ?>"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js') ?>"></script>
    <!-- <script src="js/main.js"></script> -->
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
</body>
</html>

<?php
	foreach ($data as $dat){
		$kode = $dat['kd_brg'];
		$nama = $dat['nm_brg'];
		$satuan = $dat['satuan'];
		$harga = $dat['harga'];
		$harga = $dat['harga_beli'];
		$stok = $dat['stok'];
		$pembelian = $dat['stok_min'];
		$gambar = $dat['gambar'];
		$desc = $dat['desc'];
	}
?>
<?php $id = $this->session->userdata('nama'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>B-HINEKA</title>
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- <link href="css/prettyPhoto.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/prettyPhoto.css') ?>" rel="stylesheet">
    <!-- <link href="css/price-range.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/price-range.css') ?>"" rel="stylesheet">
    <!-- <link href="css/animate.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('assets/css/animate.css') ?>" rel="stylesheet">
	<!-- <link href="css/main.css" rel="stylesheet"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css') ?>">
	<!-- <link href="css/responsive.css" rel="stylesheet"> -->
	<link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">
	
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="shortcut icon" href="<?php echo base_url('assets/images/ico/favicon.ico') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/images/ico/apple-touch-icon-144-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/images/ico/apple-touch-icon-72-precomposed.png') ?>">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/images/ico/apple-touch-icon-57-precomposed.png') ?>">
</head><!--/head-->

<body>
<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +62 821-3827-3818</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> BinekaStore@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
					<div class="logo pull-left">
							<a href="index.html"><img src="images/logo/logo.jpg" alt="" /></a>
							<a href="index.html"><img src="<?php echo base_url('assets/images/logo/logo.jpg') ?>" alt="" /></a>
						</div>
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									Indonesia
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Singapore</a></li>
									<li><a href="#">USA</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									Rupiah
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_profil/".$id;?>"><i class="fa fa-user"></i> My Account</a></li>
								<li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_checkout/".$id;?>"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_cart/".$id;?>"><i class="fa fa-shopping-cart"></i> Cart</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html" class="active">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo base_url()."index.php/hal_admin/shop/".$id;?>">Products</a></li>
										<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_checkout/".$id;?>">Checkout</a></li> 
										<li><a href="<?php echo base_url()."index.php/hal_admin/ambil_cart/".$id;?>">Cart</a></li> 
                                    </ul>
                                </li> 
								<li class="dropdown"><a href="#">My Account<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo base_url()."index.php/hal_admin/ambil_profil/".$id;?>">Profil</a></li>
										<li><a href="<?php echo base_url()."index.php/welcome/logout"; ?>">logout</a></li>
                                    </ul>
                                </li>
								<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog.html">Blog List</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#Alat">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Alat Pancing
										</a>
									</h4>
								</div>
								<div id="Alat" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Maguro</a></li>
											<li><a href="">Exori</a></li>
											<li><a href="">Kenzi</a></li>
											<li><a href="">Shimano</a></li>
											<li><a href="">Pioneer</a></li>
											<li><a href="">Relix</a></li>
											<li><a href="">Tornado</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#Ikan">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Ikan/Frozenn Food
										</a>
									</h4>
								</div>
								<div id="Ikan" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Lobster</a></li>
											<li><a href="#">Kerang</a></li>
											<li><a href="#">Bulu babi</a></li>
											<li><a href="#">Telur Ikan</a></li>
											<li><a href="#">Jenis ikan</a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#Buku">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Buku Budidaya
										</a>
									</h4>
								</div>
								<div id="Buku" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Keramba Laut</a></li>
											<li><a href="#">Keramba air tawar</a></li>
											<li><a href="#">Keramba air Payau</a></li>
											<li><a href="#">Budidaya aneka biota laut</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#Benih">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Benih/bibit
											</a>
									</h4>
								</div>
								<div id="Benih" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Maguro</a></li>
											<li><a href="">Exori</a></li>
											<li><a href="">Kenzi</a></li>
											<li><a href="">Shimano</a></li>
											<li><a href="">Pioneer</a></li>
											<li><a href="">Relix</a></li>
											<li><a href="">Tornado</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#Souvenir">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Souvenir
											</a>
										</h4>
								</div>
								<div id="Souvenir" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#Seafood">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Seafood
											</a>
										</h4>
								</div>
								<div id="Seafood" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordian" href="#Accessories">
												<span class="badge pull-right"><i class="fa fa-plus"></i></span>
												Accessories
											</a>
									</h4>
								</div>
								<div id="Accessories" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
										</ul>
									</div>
								</div>
							</div>
						</div><!--/category-products-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href=""> <span class="pull-right">(50)</span>Maguro</a></li>
									<li><a href=""> <span class="pull-right">(56)</span>Exori</a></li>
									<li><a href=""> <span class="pull-right">(27)</span>Kenzi</a></li>
									<li><a href=""> <span class="pull-right">(32)</span>Shimano</a></li>
									<li><a href=""> <span class="pull-right">(5)</span>Pioneer</a></li>
									<li><a href=""> <span class="pull-right">(9)</span>Relix</a></li>
									<li><a href=""> <span class="pull-right">(4)</span>Tornado</a></li>
								</ul>
							</div>
						</div><!--/brands_products-->
						
						<div class="price-range"><!--price-range-->
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b class="pull-left">Rp. 0</b> <b class="pull-right">Rp 10.000.000</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="images/home/shipping.jpg" alt="" />
						</div><!--/shipping-->
						
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<?php echo base_url("assets/images/Produk/$gambar")?>" alt="" />
								<h3>ZOOM</h3>
							</div>

						</div>
						<form action="<?php echo base_url()."index.php/hal_admin/tambah_cart/"?>" method="POST">
							<div class="col-sm-7">
								<div class="product-information"><!--/product-information-->
									<img src="images/product-details/new.jpg" class="newarrival" alt="" />
									<h2><?php echo $nama; ?></h2>
									<p>Produk ID: <?php echo $kode; ?></p>
									<img src="images/product-details/rating.png" alt="" />
									<span>
										<a><span>Rp</span><span><?php echo $harga; ?></span></a>
										<label>Quantity:</label>
										<input type="text" name="jumlah" value="" />
										<input type="hidden" name="gambar" value="<?php echo $gambar; ?>" />
										<input type="hidden" name="id" value="<?php echo $id; ?>"><br>
										<input type="hidden" name="kode_produk" value="<?php echo $kode; ?>"><br>
										<input type="hidden" name="nama_produk" value="<?php echo $nama; ?>"><br>
										<input type="hidden" name="satuan" value="<?php echo $satuan ?>"><br>
										<input type="hidden" name="harga" value="<?php echo $harga ?>"><br>
										<button type="submit" class="btn btn-fefault cart">
											<i class="fa fa-shopping-cart"></i>
											Add to cart
										</button>
									</span>
									<p><b>Availability:</b> In Stock</p>
									<p><b>Condition:</b> New</p>
									<p><b>Brand:</b> B-hineka</p>
									<a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
								</div><!--/product-information-->
							</div>
						</form>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li><a href="#details" data-toggle="tab">Details</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
								<li><a href="#tag" data-toggle="tab">Tag</a></li>
								<li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
							</ul>
						</div>
							
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<ul>
										<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>23 juni 2021</a></li>
									</ul>
									<p><?php echo $desc; ?></p>
									<p><b>Write Your Review</b></p>
									
									<form action="#">
										<span>
											<input type="text" placeholder="Your Name"/>
											<input type="email" placeholder="Email Address"/>
										</span>
										<textarea name="" ></textarea>
										<b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
										<button type="button" class="btn btn-default pull-right">
											Submit
										</button>
									</form>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>B</span>-hineka</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,sed do eiusmod tempor</p>
						</div>
					</div>
					<div class="col-sm-7">
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>Kami Hadir di Seluruh Pejuru- Dunia(2021 juni, Indonesia, Semarang)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Service</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Online Help</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Order Status</a></li>
								<li><a href="#">Change Location</a></li>
								<li><a href="#">FAQ’s</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quock Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Mobile</a></li>
								<li><a href="#">LAPTOPS</a></li>
								<li><a href="#">DISPLAYS / DESKTOP</a></li>
								<li><a href="#">MOTHERBOARDS</a></li>
								<li><a href="#">COMPONENTS</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privecy Policy</a></li>
								<li><a href="#">Refund Policy</a></li>
								<li><a href="#">Billing System</a></li>
								<li><a href="#">Ticket System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Shopper</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Company Information</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
				<p class="pull-left">Copyright © 2021 B-hineka All Rights Reserved By Albertus Dimas.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.Webinaja.com">Albert</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    <!-- <script src="js/jquery.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
	<!-- <script src="js/bootstrap.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="<?php echo base_url('assets/js/jquery.scrollUp.min.js') ?>"></script>
	<!-- <script src="js/price-range.js"></script> -->
	<script src="<?php echo base_url('assets/js/price-range.js') ?>"></script>
    <!-- <script src="js/jquery.prettyPhoto.js"></script> -->
    <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js') ?>"></script>
    <!-- <script src="js/main.js"></script> -->
    <script src="<?php echo base_url('assets/js/main.js') ?>"></script>
</body>
</html>